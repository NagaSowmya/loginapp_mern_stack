import axios from 'axios'
import {AsyncStorage} from 'react-native';
export const register = newUser => {
    return axios
        .post('http://192.168.0.7:5000/users/register', {
            first_name: newUser.first_name,
            last_name: newUser.last_name,
            email: newUser.email,
            password: newUser.password
        })
        .then(res => {
            console.log("Registered")
            return res;
        })
        .catch(err => {
            console.log(err)
            return err;
        })
}

export const login = user => {
    return axios
        .post('http://192.168.0.7:5000/users/login', {
            email: user.email,
            password: user.password
        })
        .then(res => {
            AsyncStorage.setItem('usertoken', res.data)
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
}