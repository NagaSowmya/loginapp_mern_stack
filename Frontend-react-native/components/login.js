import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableWithoutFeedback,
  Image,
  Alert, AsyncStorage
} from 'react-native';
import { login } from './UserFunctions'

export default class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    }
  }
  onClickListener = (viewId) => {
    Alert.alert("Alert", "Button pressed " + viewId);
  }
  async componentWillMount() {
    let token = await AsyncStorage.getItem('usertoken')
    if (token !== null) {
      this.props.navigation.navigate('Profile', token)
    }
  }


  loginButton() {
    // const { email, password } = this.state
    const user = {
      email: this.state.email,
      password: this.state.password
    }
    login(user).then(res => {
      if (res) {
        // console.log(res);
        this.props.navigation.navigate('Profile', res)
      }
    })


  }

  render() {
    const { email, password } = this.state
    return (
      <View style={styles.container}>
        <Text style={styles.txtContainer}>LOGIN</Text>
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{ uri: 'https://png.icons8.com/message/ultraviolet/50/3498db' }} />
          <TextInput style={styles.inputs}
            placeholder="Email"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            value={email}
            onChangeText={(email) => this.setState({ email })} />
        </View>

        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{ uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db' }} />
          <TextInput style={styles.inputs}
            placeholder="Password"
            secureTextEntry={true}
            value={password}
            underlineColorAndroid='transparent'
            onChangeText={(password) => this.setState({ password })} />
        </View>
        <View style={styles.btnContainer}>

          <TouchableWithoutFeedback onPress={() => this.loginButton()}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.btnContainer2}>

          <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Register')} title="Register">
            <Text style={styles.loginText}>Register</Text>
          </TouchableWithoutFeedback>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B2EE',
  },
  txtContainer: {
    marginBottom: 30,
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontFamily: 'Roboto',
    fontSize: 50

  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center'
  },

  btnContainer: {
    borderBottomColor: '#00608B',
    backgroundColor: '#00688B',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnContainer2: {
    borderBottomColor: '#009ACD',
    backgroundColor: '#009ACD',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  loginButton: {
    height: 45,
    width: 250,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderRadius: 30,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },

  loginText: {
    color: 'white',
  }
});