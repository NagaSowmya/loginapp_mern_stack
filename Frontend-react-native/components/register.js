import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableWithoutFeedback,
  Alert
} from 'react-native';
import { register } from './UserFunctions'
export default class RegisterScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
    }
  }

  onClickListener = (viewId) => {
    Alert.alert("Alert", "Button pressed " + viewId);
  }

  registerButton() {

    const user = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      password: this.state.password
    }

    register(user).then(res => {
      //  alert(JSON.stringify(res))
      if (res.status == 200) {
        this.props.navigation.navigate('Login');
      }
    })

  }

  render() {
    const { first_name, last_name, email, password } = this.state
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="first_name"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            value={first_name}
            onChangeText={(first_name) => this.setState({ first_name })} />
        </View>

        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="last_name"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            value={last_name}
            onChangeText={(last_name) => this.setState({ last_name })} />
        </View>

        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="Email"
            keyboardType="email-address"
            underlineColorAndroid='transparent'
            value={email}
            onChangeText={(email) => this.setState({ email })} />
        </View>

        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="Password"
            secureTextEntry={true}
            value={password}
            underlineColorAndroid='transparent'
            onChangeText={(password) => this.setState({ password })} />
        </View>
        <View style={styles.loginButton}>

          <TouchableWithoutFeedback onPress={() => this.registerButton()}>
            <Text style={styles.loginText}>Register</Text>
          </TouchableWithoutFeedback>

        </View>



      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00B2EE',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
    borderBottomWidth: 1,
    width: 250,
    height: 35,
    marginBottom: 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
  },
  loginButton: {
    height: 45,
    width: 250,
    backgroundColor: '#00688B',
    borderColor: '#00688B',
    // borderWidth: 1,
    borderRadius: 30,
    marginBottom: 10,
    alignItems: 'center',
    // flexDirection: 'row',
    // alignSelf: 'stretch',
    justifyContent: 'center'
  },

  loginText: {
    color: 'white',
  }
});