import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation'
import LoginScreen from "./components/login";
import RegisterScreen from "./components/register";
import Profile from "./components/profile";

const AppStack = createStackNavigator({

  Login: {
    screen: LoginScreen,
    navigationOptions: {
      // title: "Login",
      headerStyle: {
        backgroundColor: '#00B2EE'
      },
      // headerTintColor: '#fff',
      // headerTitleStyle: {
      //   fontWeight: 'bold',
      // },

    }
  },

  Register: {
    screen: RegisterScreen,
    navigationOptions: {
      title: "Register",
      headerStyle: {
        backgroundColor: '#00688B'
      },
      headerTintColor: '#FFFFFF',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  },
  Profile: {
    screen: Profile,
    navigationOptions: {
      title: "Profile",
      headerStyle: {
        backgroundColor: '#00688B'
      },
      headerTintColor: '#FFFFFF',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }
  }

});

const AppContainer = createAppContainer(AppStack);
export default class App extends Component {
  render() {
    return (
      <AppContainer />
    );
  }
}

