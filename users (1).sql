-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 30, 2019 at 05:14 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `password` varchar(250) NOT NULL,
  `created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `created`) VALUES
(1, 'sowmya', 'mallipudi', 'mnsow@gmail.com', 'abc123', '2018-11-15'),
(2, 'leela', 'bandaru', 'leela@gmail.com', 'abc123', '2018-11-16'),
(3, 'nani', 'mallipudi', 'nani@gmail.com', '$2b$10$9X/IXN07CqxiTDkfajHSPeSBaVGKIY3DOVgHnLNUPOl4ezAGJUoBW', '2018-11-16'),
(4, 'NagaSowmya', 'mallipudi', 'sowmya@gmail.com', '$2b$10$3J6nBT9EW3ldxlCBulQzde49LAOwZfSXu8mzFe94VkWteI.PmBpAO', '2018-11-16'),
(5, 'Ramaleela', 'Bandaru', 'leela1@gmail.com', '$2b$10$vaSuHhFNDjaQS1/biUDjYu09FkLKlch0pMb1ID3Q7Xql0Bj4o2fR.', '2018-11-16'),
(6, 'Sowmya', 'Mallipudi', 'mnsowmya93@gmail.com', '$2b$10$TTOzi9mahMP6YXe5vaMhmed.828oTeWPu.uw9d0IZXXXPG9gJ6e6G', '2018-12-02'),
(7, 'Bruno', 'Mallipudi', 'bruno@gmail.com', '$2b$10$FmKvHOaSEmpFe7KfZ8S0c..Vd6egASpoeOqpkqb6V78v4P7I5KbRu', '2018-12-02'),
(8, 'p', 'leela', 'leelabandaru2@gmail.com', '$2b$10$2EJ8lTDU1WGLMgvSa/u4Ke8xr7TXJm8TIjAgVbNtgZfFWQ1zJOXLK', '2018-12-10'),
(23, 'Test', 'Test1', 'test@test.com', '$2b$10$Lb1dV1P1ixvuFRiRX7XtW.xJxQGa/Zh5izkC3pfwZUhGfw524gLpy', '2019-03-30'),
(10, 'abc', 'abc', 'abc@gmail.com', '$2b$10$0B7NgDruUVuk7Cg.CRuWJ.NiawYuRCx9LKGY94Mag2VkDVa3PbIq6', '2019-01-18'),
(11, 'Test1', 'T1', 'test1@test.com', '$2b$10$PxnSbHX0H6St0PooiC9zdevwocEIpp602RjWqQzv1S51rv9PO9Az6', '2019-01-22'),
(12, 'P', 'Shuka', 'Leela@shuka.com', '$2b$10$.a5sZV3KMQr7WwEGV7i0iObYgP85wcj03Ocv3GVmS6fHDGox.gpLC', '2019-01-22'),
(13, 'sowmya', 'Mallipudi', 'sowmya93123@gmail.com', '$2b$10$NTIduXO7m8STHOGKCRkrde4NdK5sxd6Jioz1.dBBq8Gnz85iaDsZ6', '2019-03-13'),
(14, 'srikar', 'Mallipudi', 'srim@gmail.com', '$2b$10$OaU6k171zgobmEIsPYpHNe3/H9hZf//jHClqsV0wXJjbNbUGv1KDy', '2019-03-13'),
(15, 'lashmi', 'mm', 'lashmi@abc.com', '$2b$10$RHh6tJrZHP45.oox/z1mYunmyu1cge8gizOu1RBPFQ/x6UcTgmDzG', '2019-03-13'),
(16, 'test123', '123', 'test1234@test.com', '$2b$10$PPULifPMA5K.wc2S9fFEmOdbTzNQkJvrxYoggdtZjlMh0uh6IWe3q', '2019-03-30'),
(17, '123', '123', '123@123.com', '$2b$10$K.5/UWWS0b1Alr0eoICP8ujj4MqJl1FRN/NSfK6EPf9gtkP5Szo3W', '2019-03-30'),
(18, '123', '1234', '1234@1234.com', '$2b$10$OhgqlZ0CcN4gi1Wn7CERH.gLGMxln6YY449xiGSUhmu5E2M.OC6Ti', '2019-03-30'),
(19, 'T123', '123', 'T123@12.co', '$2b$10$P73mY61X2iLHh/hkj5CJpOaLe07You.bw8MTnhiTcw0bZ.Fxl3zmi', '2019-03-30'),
(20, 'T12', '12', 'T1234@1.co', '$2b$10$JjRoonq./CMzovWcrAeLkO8GeUzMfyvr563Wt0kseoNls0LHNiRKm', '2019-03-30'),
(21, 'Test', 'Test1', 'Test@test1.com', '$2b$10$h0xQxNxnDSwNfQRi1b49ruPyCrvifNFYtJoYtx.9tU59nRAHjMn0W', '2019-03-30'),
(22, 'Test11', 'Tes111', 't11@t11.co', '$2b$10$bNbulJB.ASGDZp9jTePEBeVCdOAHgOFxYWv6.RlJUs9OQ2wWJC.Iq', '2019-03-30'),
(24, 'Abc', '123', 'abc@123.com', '$2b$10$vHAOPHS8wYGS1q4c5aG/8.ITfgfo3IFxugfXwUUh.I7fFq3SEJyC2', '2019-03-30');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
