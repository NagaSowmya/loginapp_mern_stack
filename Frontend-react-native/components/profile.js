import React, { Component } from 'react';
import { Text, View, Button, AsyncStorage, StyleSheet,TouchableWithoutFeedback } from 'react-native';
import jwt_decode from 'jwt-decode'
class Profile extends Component {
    constructor() {
        super()
        this.state = {
            first_name: '',
            last_name: '',
            email: ''
        }
    }

    componentDidMount() {

        // alert(this.props.navigation.state.params)
        const token = this.props.navigation.state.params
        const decoded = jwt_decode(token)
        this.setState({
            first_name: decoded.first_name,
            last_name: decoded.last_name,
            email: decoded.email
        })


    }
    render() {

        return (
            <View style={styles.container}>
                <Text style={styles.txtContainer}>Welcome to your Profile!!!</Text>
                <Text style={styles.smallTxtContainer}>First Name:{this.state.first_name}</Text>
                <Text style={styles.smallTxtContainer}>Last Name:{this.state.last_name}</Text>
                <Text style={styles.smallTxtContainer}>Email:{this.state.email}</Text>

                <View style={styles.btnContainer}>

                    <TouchableWithoutFeedback onPress={() => {
                        AsyncStorage.clear();
                        this.props.navigation.navigate('Login')
                    }}>
                        <Text style={styles.loginText}>Logout</Text>
                    </TouchableWithoutFeedback>
                </View>
                
            </View>
        );
    }
}
export default Profile;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00B2EE',
    },
    txtContainer: {
        marginBottom: 50,
        color: '#E6E8FA',
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        fontSize: 25

    },
    smallTxtContainer: {
        marginBottom: 25,
        color: '#162252',
        fontFamily: 'Roboto',
        fontSize: 22,
        fontWeight: 'bold',

    },


    btnContainer: {
        borderBottomColor: '#00608B',
        backgroundColor: '#00688B',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnContainer2: {
        borderBottomColor: '#009ACD',
        backgroundColor: '#009ACD',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginButton: {
        height: 45,
        width: 250,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        // borderWidth: 1,
        borderRadius: 30,
        marginBottom: 10,
        alignItems: 'center',
        // flexDirection: 'row',
        // alignSelf: 'stretch',
        justifyContent: 'center'
    },

    loginText: {
        color: 'white',
    }
});